const app = require('./app');
const dotenv = require('dotenv');
dotenv.config();
const mongoose = require('mongoose');

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD
)

mongoose.connect(DB)
.then((con) => {
    console.log("DB connected successfully")
})
.catch((error) => {
    console.log(error.message);
})


app.listen(3000, () => {
    console.log("Connected to port 3000")
})