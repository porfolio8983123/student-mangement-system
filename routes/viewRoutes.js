const express = require('express');
const router = express.Router();
const viewController = require('./../controllers/viewController');

router.get('/login',viewController.getLoginForm);
router.get('/dashboard',viewController.getDashboard);

module.exports = router;