const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
    std_id: {
        type: Number,
        required: [true,'Please provide student enrollment number!'],
        unique:true
    },
    name: {
        type: String,
        required:[true,'Please tell us your name!']
    },
    gender: {
        type: String,
        required:[true,'Please provide your gender!']
    },
    email: {
        type: String,
        required:[true,'Please provide your email!'],
        unique:true,
        lowercase: true
    },
    photo: {
        type:String, 
        default: 'default.png'
    },
    semester:{
        type: Number,
        required: [true,'Please provide the semester of a student']
    },
    dateOfBirth: {
        type: String,
        require:[true,'Please provide student date of birth!']
    },
    phoneNumber: {
        type: Number,
        required: [true,'Please provide student phone number!'],
        minlength:8
    },
    CID: {
        type: Number,
        required:[true,'Please provide student CID!'],
        minlength:11,
        unique:true
    },
    password: {
        type: String, 
        required:[true,'Please provide a password!'],
        minlength: 8,
        select: false
    },
    passwordChanged: {
        type: Boolean,
        default:false
    },
    dzongkhag: {
        type: String,
        required:[true,'Please provide student dzongkhag!']
    },
    gewog: {
        type: String,
        required: [true,'Please provide student gewog']
    },
    chiwog: {
        type: String,
        required: [true,'Please provide student chiwog']
    },
    householdNumber: {
        type: String,
        required:[true,'Please provide household number']
    },
    thramNo: {
        type: Number,
        required:[true,'Please provide thram no!']
    },
    course: {
        type: String,
        required:[true,'Please provide the student major course!']
    }
})

userSchema.pre('save',async function(next) {
    if (!this.isModified('password')) return next()

    this.password = await bcrypt.hash(this.password,12);
    
    next();
})

userSchema.methods.correctPassword = async function(candidatePassword,userPassword) {
    return await bcrypt.compare(candidatePassword,userPassword); 
}

const User = mongoose.model('User',userSchema);
module.exports = User;