const express = require('express')
const app = express()
const userRouter = require('./routes/userRoutes');
const path = require('path');
const viewRouter = require('./routes/viewRoutes');

app.use(express.json());
app.use('/api/v1/users',userRouter);
app.use('/',viewRouter)


module.exports = app;