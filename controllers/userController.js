const User = require('./../models/userModel');

exports.getAllUsers = async (req,res,next) => {
    try {
        const users = await User.find();
        res.status(200).json({data:users,status:'success'});
    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

exports.createUser = async (req,res) => {
    try {
        const user = await User.create(req.body);
        res.json({data:user,status:'success'});
    } catch (error) {
        res.status(500).json({err:error.message});
    }
}