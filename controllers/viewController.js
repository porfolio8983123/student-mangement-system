const path = require('path')

exports.getLoginForm = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','login.html'));
}

exports.getDashboard = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','dashboard.html'));
}