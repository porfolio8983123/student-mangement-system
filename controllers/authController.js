const User = require('./../models/userModel');
const jwt = require('jsonwebtoken')
const AppError = require('./../utils/appError');

const signToken = (id) => {
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn: process.env.JWT_EXPIRES_IN
    })
}

const createSendToken = (user,statusCode,res) => {
    const token = signToken(user._id)
    console.log(token);

    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    }

    res.cookie('jwt',token,cookieOptions)

    res.status(statusCode).json({
        status: 'success',
        token,
        data: {
            user
        }
    })
}

exports.login = async (req,res,next) => {
    try {
        const {email,password} = req.body;
        console.log(email,password)
        
        if (!email || !password) {
            return next(new AppError('Please provide an email and password!'))
        }

        const user = await User.findOne({email}).select('+password');

        console.log(user);

        if (!user || !(await user.correctPassword(password,user.password))) {
            return next(new AppError('Incorrect email or password',401));
        }

        createSendToken(user,200,res);

    } catch (error) {
        console.log(error.message);
    }
}